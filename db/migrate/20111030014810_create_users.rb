class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.integer :id
      t.string :name
      t.string :description
      t.string :avatar
      t.string :address
      t.string :twitter

      t.timestamps
    end
  end
end
