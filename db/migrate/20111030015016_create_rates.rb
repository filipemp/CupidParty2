class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.integer :id_rater
      t.integer :id_rated
      t.integer :rating

      t.timestamps
    end
  end
end
