class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :message
      t.integer :id_sender
      t.integer :id_receiver

      t.timestamps
    end
  end
end
