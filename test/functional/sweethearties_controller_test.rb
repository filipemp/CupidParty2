require 'test_helper'

class SweetheartiesControllerTest < ActionController::TestCase
  setup do
    @sweethearty = sweethearties(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sweethearties)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sweethearty" do
    assert_difference('Sweethearty.count') do
      post :create, sweethearty: @sweethearty.attributes
    end

    assert_redirected_to sweethearty_path(assigns(:sweethearty))
  end

  test "should show sweethearty" do
    get :show, id: @sweethearty.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sweethearty.to_param
    assert_response :success
  end

  test "should update sweethearty" do
    put :update, id: @sweethearty.to_param, sweethearty: @sweethearty.attributes
    assert_redirected_to sweethearty_path(assigns(:sweethearty))
  end

  test "should destroy sweethearty" do
    assert_difference('Sweethearty.count', -1) do
      delete :destroy, id: @sweethearty.to_param
    end

    assert_redirected_to sweethearties_path
  end
end
