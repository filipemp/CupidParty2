class SweetheartiesController < ApplicationController
  # GET /sweethearties
  # GET /sweethearties.json
  def index
    @sweethearties = Sweethearty.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sweethearties }
    end
  end

  # GET /sweethearties/1
  # GET /sweethearties/1.json
  def show
    @sweethearty = Sweethearty.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sweethearty }
    end
  end

  # GET /sweethearties/new
  # GET /sweethearties/new.json
  def new
    @sweethearty = Sweethearty.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sweethearty }
    end
  end

  # GET /sweethearties/1/edit
  def edit
    @sweethearty = Sweethearty.find(params[:id])
  end

  # POST /sweethearties
  # POST /sweethearties.json
  def create
    @sweethearty = Sweethearty.new(params[:sweethearty])

    respond_to do |format|
      if @sweethearty.save
        format.html { redirect_to @sweethearty, notice: 'Sweethearty was successfully created.' }
        format.json { render json: @sweethearty, status: :created, location: @sweethearty }
      else
        format.html { render action: "new" }
        format.json { render json: @sweethearty.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sweethearties/1
  # PUT /sweethearties/1.json
  def update
    @sweethearty = Sweethearty.find(params[:id])

    respond_to do |format|
      if @sweethearty.update_attributes(params[:sweethearty])
        format.html { redirect_to @sweethearty, notice: 'Sweethearty was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @sweethearty.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sweethearties/1
  # DELETE /sweethearties/1.json
  def destroy
    @sweethearty = Sweethearty.find(params[:id])
    @sweethearty.destroy

    respond_to do |format|
      format.html { redirect_to sweethearties_url }
      format.json { head :ok }
    end
  end
end
